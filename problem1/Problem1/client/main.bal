import ballerina/io;
import ballerina/http;

final http:Client clientEndpoint = check new ("http://localhost:6000");
public function main() returns error? {
    io:println("Hello World!");
    json studentprof = {
                username: "mafwila@thebuho",
                lastname: "thebuho",
                firstname: "clive",
                preferred_formats: ["audio", "video", "text"],
                past_subjects: [
                        {
                        "course": "Algorithms",
                        score: "A+"
                        },
                        {
                        course: "Programming I",
                        score: "B+"
                        }
                    ]
                };

    // string value = check clientEndpoint->get("/hello/Update/'clive'/21");
    json value = check clientEndpoint->post("/createLearnerProfile",studentprof);
    io:println(value);

    json updatedstudentprof = {
                username: "mafwila@thebuho",
                preferred_formats: ["audio", "video"],
                past_subjects: [
                        {
                        "course": "Algorithms",
                        score: "C+"
                        },
                        {
                        course: "Programming I",
                        score: "B+"
                        },
                        {
                        course: "Mathematics",
                        score: "C+"
                        },
                        {
                        course: "Database",
                        score: "B+"
                        }
                    ]
                };
    io:println("------------------------Updated Learner-------------------");
// sends and prints back the updated user
    json updatedvalue = check clientEndpoint->post("/updateLeanerProfile",updatedstudentprof);
    io:println(updatedvalue);
}
