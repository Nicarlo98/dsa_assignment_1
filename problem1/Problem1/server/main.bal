import ballerina/io;
import ballerina/http;
import ballerinax/mongodb;

mongodb:ClientConfig mongoConfig = {
    host: "localhost",
    port: 27017,
    // username: <DB_USERNAME>,
    // password: <DB_PASSWORD>,
    options: {sslEnabled: false, serverSelectionTimeout: 5000}
};

mongodb:Client mongoClient = check new (mongoConfig, "DSPAssign");
//variable

service / on new http:Listener(6000) {

    resource function post createLearnerProfile(@http:Payload json learnerprofile) returns json|error? {
        io:println(learnerprofile.toJsonString());
        map<json> stprofile = <map<json>>learnerprofile;

        checkpanic mongoClient->insert(stprofile,"learner_profile");
        mongoClient->close();
        return learnerprofile;
    }

    resource function post updateLeanerProfile(@http:Payload json newLprofile)returns json|error? {
         string msg = " ";
        map<json> updatedprof = <map<json>>newLprofile;
        map<json>|error updatefilter = {"username":checkpanic updatedprof.username};
        // first value is the updated data, second is collection name, (), forth value is the filter or the key , true
            
       if (updatefilter is error) {
           io:println("error");
       }
       else {
           int reply = checkpanic  mongoClient->update(updatedprof, "learner_profile", (), updatefilter , true);
           
        if (reply > 0 ) {
            io:println("Modified count: '" + reply.toString() + "'.") ;
            msg = "succesfully Updated!";
        } else {
            io:println("Error: Nothing modified.");
            msg = "error: nothing Updated!";
        }
       }
        json responds={message:msg};
        return responds;
    }

    // resource function get Update/[string name]/[int age]()returns string {
    //     io:println(name,age);
    //     return "Hello world";
    // }
}