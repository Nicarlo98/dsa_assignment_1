import ballerina/http;
import ballerina/io;
import ballerinax/mongodb;

mongodb:ClientConfig mongoConfig = {
    host: "localhost",
    port: 27017,
    // username: <DB_USERNAME>,
    // password: <DB_PASSWORD>,
    options: {sslEnabled: false, serverSelectionTimeout: 5000}
};

mongodb:Client mongoClient = check new (mongoConfig, "DSPAssign");

service / on new http:Listener(6000) {

    resource function post createLearnerProfile(@http:Payload json learnerprofile) returns json|error? {
        io:println(learnerprofile.toJsonString());
        map<json> stprofile = <map<json>>learnerprofile;

        checkpanic mongoClient->insert(stprofile,"learner_profile");
        mongoClient->close();
        return learnerprofile;
    }

    resource function post updateLeanerProfile(@http:Payload json newLprofile){

        map<json> updatedproff = <map<json>>newLprofile;
        map<json> updatefilter = {"username":newLprofile.username}
        // first value is the updated data, second is collection name, (), forth value is the filter or the key , true
            int response = checkpanic mongoClient->update(updatedproff, "learner_profile", (), updatefilter , true);
        string message;
        if (response > 0 ) {
            io:println("Modified count: '" + response.toString() + "'.") ;
            message = "succesfully Updated!";
        } else {
            io:println("Error: Nothing modified.");
            message = "error: nothing Updated!";
        }
        return {message:message};
    }

    // resource function get Update/[string name]/[int age]()returns string {
    //     io:println(name,age);
    //     return "Hello world";
    // }
}