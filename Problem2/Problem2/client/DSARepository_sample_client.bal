import ballerina/io;
import ballerina/grpc;
DSARepositoryClient ep = check new ("http://localhost:9090");

public function main() {
    io:println("*********** Repo Assignment ********");
    io:println("1. ADD New Function");
    io:println("2. ADD Functions");
    io:println("3. Delete Function");
    io:println("4. Show Function");
    io:println("5. Show All Functions");
    io:println("6. Show all with Criteria Function");
    io:println("---------------------------------------");
    string choose = io:readln("Enter Option");

    if (choose === "1"){
        add_new_fn();
    }else if (choose === "2") {
        add_fns();
    }else if (choose === "3") {
        delete_fn();
    }else if (choose === "4") {
        show_fn();
    }else if (choose === "5") {
        show_all_fns();
    }else if (choose === "6") {
        show_all_with_criteria();
    }

}

public function add_new_fn() {
    FUNCTION fncts = {fn_data: "hsghdfagdfhagdfgahsfdhg",
    fn_metadata: {
        developer: {
            email: "clive@mafwila",
            fullname: "clive thebuho"
            },keywords: ["hagshhghsahf","ajsghgshasg","jahskjahskjshk"],
            language: "Ballerina"
        }
    };
    var respond =  ep->add_new_fn(fncts);
    if respond is grpc:Error {
        io:println(respond.toString());
    } else {
        io:println(respond);
    }
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var hello = (back === "1")?main():add_new_fn();

}

public function add_fns(){
    FUNCTION [] gt = [];

    FUNCTION fncts = {fn_data: "hsghdfagdfhagdfgahsfdhg",
    fn_metadata: {
        developer: {
            email: "clive@thebuho",
            fullname: "clive thebuho"
            },keywords: ["hagshhghsahf","ajsghgshasg","jahskjahskjshk"],
            language: "Ballerina"
        }
    };

    gt.push(fncts);
    gt.push(fncts);
    gt.push(fncts);
    gt.push(fncts);
    gt.push(fncts);

    // foreach FUNCTION item in gt {
    //     var respond = check ep->add_fns(item);
    //     if respond is grpc:Error {
    //         io:println(respond.toString());
    //     } else {
    //         io:println(respond);
    //     }
    // }
    
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var hello = (back === "1")?main():add_new_fn();
}

public function delete_fn() {

    var respond =  ep->delete_fn(" hhfdgjggjjgfdhgfdjgjf");
    if respond is grpc:Error {
        io:println(respond.toString());
    } else {
        io:println(respond);
    }
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var hello = (back === "1")?main():add_new_fn();
}

public function show_fn() {
    show_all_fn_ID versn_id = {"version":2,fn_id:"hellklhjhsdkjgs"};

    var respond =  ep->show_fn(versn_id);
    if respond is grpc:Error {
        io:println(respond.toString());
    } else {
        io:println(respond);
    }
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var hello = (back === "1")?main():add_new_fn();
}

public function show_all_fns() {
        var respond =  ep->show_all_fns("fdgfhgjkhhjdfgjkjhdfghghkhjhh");
    if respond is grpc:Error {
        io:println(respond.toString());
    } else {
        io:println(respond);
    }
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var hello = (back === "1")?main():add_new_fn();
}

public function show_all_with_criteria() {
    string [] criteria = ["gjgsdjjfhgjsgfsdf","gfjdsgfdjsgfhjgdfhj","dghfgdjsfsshdgjsf","sgdhdsgfsgjdjdgf"];

    // foreach string item in criteria {
    //     var respond = check ep->show_all_with_criteria(item);
    //     if respond is grpc:Error {
    //         io:println(respond.toString());
    //     } else {
    //         io:println(respond);
    //     }
    // }
    // check ep->complete();
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var hello = (back === "1")?main():add_new_fn();
}
